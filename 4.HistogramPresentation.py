# Define a function that takes as parameters the dataset and two integers.
# The first integer determines a feature (column) of the dataset, while the second one determines
# the number of bins of the histogram. Function should return the chosen feature's histogram.

from matplotlib import pyplot as plt


def histogram(dataset, feature, bins):
    selectedColumn = dataset[:, feature]
    plt.hist(selectedColumn, bins)
    # Add title, axis names and present histogram
    plt.title('Feature\'s histogram')
    plt.xlabel('Value')
    plt.ylabel('Times')
    plt.show()


histogram(datasetClean, 11, 10)
