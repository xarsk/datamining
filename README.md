This project contains a part of my answers given to a University assignment, 
on Data Mining course of Harokopio University, by Prof. Iraklis Varlamis and John Violos.
The Wine Quality Dataset (https://archive.ics.uci.edu/ml/datasets/Wine+Quality) 
was used and the code ran on Google Collabora.