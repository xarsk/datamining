from numpy import genfromtxt
from google.colab import drive

drive.mount('/content/gdrive')

path = 'gdrive/My Drive/Dataset/dataset_part_one.csv'
dataset = genfromtxt(path, delimiter=',')
