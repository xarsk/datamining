# How much has the accuracy of prediction improved with any of the previous classifiers
# compared to a Dummy classifier that always predicts the most common training dataset label?

from sklearn.dummy import DummyClassifier

dummy_clf = DummyClassifier(strategy="most_frequent")
dummy_clf.fit(X_training, y_training)

print("Dummy Classifier score is", dummy_clf.score(X_testing, y_testing))

# Dummy Classifier score is 0.6468769325912183
