#  Use DBSCAN method, try for different eps, find the different number of clusters
#  formed and evaluate the results with the Silhouette metric.

from sklearn.cluster import DBSCAN

dbscan = DBSCAN(eps=0.3).fit(datasetClean[:, 0:11])
dbscan_labels = dbscan.labels_
clusters = len(set(dbscan_labels)) - (1 if -1 in dbscan_labels else 0)

print("No. of clusters =", clusters)
print(metrics.silhouette_score(datasetClean[:, 0:11], dbscan_labels, metric='euclidean'))


# No. of clusters = 14
# -0.5989879418558807
# Seems like items are placed in wrong clusters

