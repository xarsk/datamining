# We import the two Dataset parts and we merge them in a new array.
# Columns (0-4) will have the data of part 1, while columns (5-11) will have the data of part 2.

import numpy as np

dataset1 = genfromtxt('gdrive/My Drive/Dataset/dataset_part_one.csv', delimiter=',')
dataset2 = genfromtxt('gdrive/My Drive/Dataset/dataset_part_two.csv', delimiter=',')

datasetFull = np.concatenate((dataset1, dataset2), axis=1)
print(datasetFull)

# [[  nan   nan   nan ...   nan   nan   nan]
#  [ 7.    0.27  0.36 ...  0.45  8.8   6.  ]
#  [ 6.3   0.3   0.34 ...  0.49  9.5   6.  ]
#  ...
#  [ 6.5   0.24  0.19 ...  0.46  9.4   6.  ]
#  [ 5.5   0.29  0.3  ...  0.38 12.8   7.  ]
#  [ 6.    0.21  0.38 ...  0.32 11.8   6.  ]]
