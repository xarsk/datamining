# "sulphates" feature won't be able to be measured in the feature using chemical analysis, but we have enough records
# to be able to predict it. Try to predict it using regression techniques. Rate each regression model with
# Mean absolute error and Mean squared error metrics. Try Normalizing the features.

from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error

workingDataset = np.zeros((datasetClean.shape[0], 11))
normDataset = np.zeros((datasetClean.shape[0], 11))

workingDataset[:, 0:9] = datasetClean[:, 0:9]
workingDataset[:, 9:11] = datasetClean[:, 10:12]

print(workingDataset)
# X = features
SX = datasetClean[:, 0:9]
# y = target
Sy = datasetClean[:, 9]

SX_training, SX_testing, Sy_training, Sy_testing = train_test_split(SX, Sy, test_size=0.33, random_state=0)

clf = svm.SVR()
clf.fit(SX_training, Sy_training)

maber = mean_absolute_error(Sy_testing, clf.predict(SX_testing))
msqer = mean_squared_error(Sy_testing, clf.predict(SX_testing))

print("Mean absolute error:", maber)
print("Mean squared error:", msqer)

# ---- Normalized data

normDataset = workingDataset
SXn = normDataset[:, 0:9]
Syn = normDataset[:, 9]

normDataset[:, 0:11] = preprocessing.normalize(workingDataset[:, 0:11], axis=0, norm='l2')

SXn_training, SXn_testing, Syn_training, Syn_testing = train_test_split(SXn, Syn, test_size=0.33, random_state=0)

clf = svm.SVR()
clf.fit(SXn_training, Syn_training)

mabern = mean_absolute_error(Syn_testing, clf.predict(SXn_testing))
msqern = mean_squared_error(Syn_testing, clf.predict(SXn_testing))

print("Mean absolute error (norm):", mabern)
print("Mean squared error (norm):", msqern)

# [[ 7.    0.27  0.36 ...  3.    8.8   1.  ]
#  [ 6.3   0.3   0.34 ...  3.3   9.5   1.  ]
#  [ 8.1   0.28  0.4  ...  3.26 10.1   1.  ]
#  ...
#  [ 6.5   0.24  0.19 ...  2.99  9.4   1.  ]
#  [ 5.5   0.29  0.3  ...  3.34 12.8   1.  ]
#  [ 6.    0.21  0.38 ...  3.26 11.8   1.  ]]
# Mean absolute error: 0.08217558116239433
# Mean squared error: 0.011273086576090503
# Mean absolute error (norm): 0.001712110055356442
# Mean squared error (norm): 3.951772310283691e-06