# Define a function that takes as parameters the dataset and an integer.
# The integer determines a feature (column) of the dataset.
# Function should find and return the outliers, using Interquartile Range method (IQR).
# IQR is calculated as the value range of a feature that is bigger than the 25% percintile (Q1)
# and smaller than the 75% percintile (Q3). So, IQR = Q3 - Q1.
# A value is considered an outlier when is smaller than Q1 - 1.5 * IQR or bigger than Q3 + 1.5 * IQR.


from scipy import stats

feature = 8
selectedColumn = datasetClean[:, feature]

# IQR
q75, q25 = np.percentile(selectedColumn, [75, 25])
iqr = q75 - q25
print("IQR for column ", feature, " is ", iqr)
top = q75 + (1.5 * iqr)
bottom = q25 - (1.5 * iqr)

outliers = selectedColumn[(selectedColumn > top) | (selectedColumn < bottom)]
print("Using IQR, outliers are:\n", outliers)
print("Top limit = ", top, ", bottom limit =", bottom)

# Z-score (Bonus)
zscore = np.abs(stats.zscore(selectedColumn))
threshold = 3
print("Using z-score, outliers are positioned in rows:\n", np.where(zscore > threshold)[0])

# Creating dataset without outliers for later bonus tasks (Later bonus)
datasetWOoutlier = np.delete(datasetClean, (outliers), axis=1)
for outlier in outliers:
    datasetWOoutlier = np.delete(datasetClean, (outlier), axis=0)



# IQR for column  8  is  0.18999999999999995
# Using IQR, outliers are:
#  [3.69 3.63 3.72 3.61 3.64 3.64 3.72 3.72 3.58 3.58 3.66 3.59 2.74 3.82
#  3.81 3.65 3.65 3.59 3.77 3.62 3.63 3.58 3.58 3.65 3.74 2.8  3.6  3.6
#  2.72 3.6  2.79 2.79 3.57 3.8  3.6  3.6  3.68 3.63 3.63 2.77 3.63 3.6
#  3.6  3.61 3.61 3.59 3.79 3.59 3.68 3.59 3.66 3.7  3.74 3.8  3.57 3.57
#  3.57 3.65 3.58 2.8  3.77 3.76 3.69 3.66 3.59 2.79 3.75 3.63 3.75 3.76
#  3.66 3.66 2.8  3.67 3.57]
# Top limit =  3.5649999999999995 , bottom limit = 2.8049999999999997
# Using z-score, outliers are positioned in rows:
#  [  72  250  830  834 1014 1250 1255 1335 1352 1385 1649 1681 1900 2036
#  2078 2321 2369 2399 2646 2711 2771 2872 2964 3025 3128 3556 4109 4259
#  4470 4565 4567 4744]