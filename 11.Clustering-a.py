# We are trying to store wines in a number of K-warehouses. Wines will be grouped based in the similarity
# of their chemical metrics. Use k-means method to find which records (wine labels) will be clustered together.
# Using elbow method, find the best number of K clusters. For the selected K, rate the resulted clustering using
# silhouette metric.

from sklearn.cluster import KMeans
from sklearn import metrics
from scipy.spatial.distance import cdist

kmeans = KMeans(n_clusters=12, random_state=0).fit(datasetClean[:, :11])
# Excluding the quality feature since the clustering is based on wine's substances
kmeans_labels = kmeans.labels_

# print(kmeans.cluster_centers_)

cluster_1 = np.empty([0, 11])
cluster_2 = np.empty([0, 11])
cluster_3 = np.empty([0, 11])
cluster_4 = np.empty([0, 11])

for label_i, record_i in zip(kmeans_labels, datasetClean[:, :11]):
    if label_i == 0:
        cluster_1 = np.append(cluster_1, [record_i], axis=0)
    if label_i == 1:
        cluster_2 = np.append(cluster_2, [record_i], axis=0)
    if label_i == 2:
        cluster_3 = np.append(cluster_3, [record_i], axis=0)
    if label_i == 3:
        cluster_4 = np.append(cluster_4, [record_i], axis=0)

print("Wines will be splitted in the following clusters:\n")
print("Cluster 1 contains:\n", cluster_1, "\n")
print("Cluster 2 contains:\n", cluster_2, "\n")
print("Cluster 3 contains:\n", cluster_3, "\n")
print("Cluster 4 contains:\n", cluster_4, "\n")

# Elbow Method
K = np.arange(1, 5)
inertias = np.empty([0])
for i in K:
    kmeans = KMeans(n_clusters=i).fit(datasetClean)
    inertias = np.append(inertias, kmeans.inertia_)
plt.plot(K, inertias, 'bo-')
plt.xlabel("No of clusters (K)")
plt.ylabel("Sum of squared distances")
plt.title("The Elbow Method")
plt.show

# Silhouette

from sklearn import metrics

kmeans = KMeans(n_clusters=2).fit(datasetClean)
print("Silhouette score is", metrics.silhouette_score(datasetClean, kmeans.labels_, metric='euclidean'))

# Wines will be splitted in the following clusters:
#
# Cluster 1 contains:
#  [[ 6.2   0.45  0.26 ...  3.27  0.52  9.8 ]
#  [ 6.2   0.46  0.25 ...  3.25  0.52  9.8 ]
#  [ 7.3   0.22  0.3  ...  3.33  0.46  9.5 ]
#  ...
#  [ 6.3   0.36  0.5  ...  3.2   0.51  9.6 ]
#  [ 6.    0.32  0.46 ...  3.24  0.49  9.6 ]
#  [ 6.5   0.28  0.38 ...  3.03  0.42 13.1 ]]
#
# Cluster 2 contains:
#  [[ 6.8   0.26  0.42 ...  3.47  0.48 10.5 ]
#  [ 7.2   0.32  0.36 ...  3.1   0.71 12.3 ]
#  [ 6.2   0.12  0.34 ...  3.42  0.51  9.  ]
#  ...
#  [ 5.7   0.21  0.32 ...  3.24  0.46 10.6 ]
#  [ 6.5   0.23  0.38 ...  3.29  0.54  9.7 ]
#  [ 6.5   0.24  0.19 ...  2.99  0.46  9.4 ]]
#
# Cluster 3 contains:
#  [[ 6.3   0.3   0.34 ...  3.3   0.49  9.5 ]
#  [ 6.2   0.32  0.16 ...  3.18  0.47  9.6 ]
#  [ 6.3   0.3   0.34 ...  3.3   0.49  9.5 ]
#  ...
#  [ 5.9   0.32  0.26 ...  3.24  0.36 10.7 ]
#  [ 6.1   0.32  0.28 ...  3.15  0.36 11.45]
#  [ 6.8   0.22  0.36 ...  3.04  0.54  9.2 ]]
#
# Cluster 4 contains:
#  [[7.   0.27 0.36 ... 3.   0.45 8.8 ]
#  [7.   0.27 0.36 ... 3.   0.45 8.8 ]
#  [6.7  0.23 0.39 ... 3.11 0.36 9.4 ]
#  ...
#  [6.5  0.33 0.38 ... 3.14 0.5  9.6 ]
#  [6.6  0.34 0.4  ... 3.15 0.5  9.55]
#  [6.6  0.32 0.36 ... 3.15 0.46 9.6 ]]
# Silhouette score is 0.5061838383889321

