# 1. Splitting the dataset in 67% training and 33% testing.
# 2. Training DT, SVM, Bayes, ANN classifiers.
# 3. Evaluating the testing part and presenting precision, recall and f-measure for each classifier.
# 4. Which classifier has the best results?
# Bonus: If classifier is trained in a training dataset that is outlier free,
# there will be better predictions in testing dataset.

from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_recall_fscore_support
from sklearn import svm
from sklearn import tree
from sklearn.naive_bayes import GaussianNB
from sklearn.neural_network import MLPClassifier
from prettytable import PrettyTable  # http://zetcode.com/python/prettytable/

dataset = datasetClean
# Replace above line with the following, to run without outliers (bonus)
# dataset=datasetWOoutlier

# 1 ---- Splitting Dataset
# X = features
X = dataset[:, 0:11]

# y = target
y = dataset[:, 11]

X_training, X_testing, y_training, y_testing = train_test_split(X, y, test_size=0.33, random_state=0)

# 2 ---- Training Part
clf_DT = tree.DecisionTreeClassifier()
clf_DT = clf_DT.fit(X_training, y_training)
DT_total = clf_DT.score(X_testing, y_testing)

clf_svm = svm.SVC(gamma='scale')
clf_svm.fit(X_training, y_training)
svm_total = clf_svm.score(X_testing, y_testing)

gnb = GaussianNB()
gnb.fit(X_training, y_training)
gnb_total = gnb.score(X_testing, y_testing)

ann = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
ann.fit(X_training, y_training)
ann_total = ann.score(X_testing, y_testing)

# 3 ---- Evaluation
clf_DT_scores = precision_recall_fscore_support(y_testing, clf_DT.predict(X_testing), average="binary")
clf_svm_scores = precision_recall_fscore_support(y_testing, clf_svm.predict(X_testing), average="binary")
gnb_scores = precision_recall_fscore_support(y_testing, gnb.predict(X_testing), average="binary")
ann_scores = precision_recall_fscore_support(y_testing, ann.predict(X_testing), average="binary")

tableDefaults = PrettyTable()
tableDefaults.field_names = ["Classifier Name", "Precision", "Recall", "F-score", "Score"]
tableDefaults.add_row(["DT", clf_DT_scores[0], clf_DT_scores[1], clf_DT_scores[2], DT_total])
tableDefaults.add_row(["SVM", clf_svm_scores[0], clf_svm_scores[1], clf_svm_scores[2], svm_total])
tableDefaults.add_row(["GNB", gnb_scores[0], gnb_scores[1], gnb_scores[2], gnb_total])
tableDefaults.add_row(["ANN", ann_scores[0], ann_scores[1], ann_scores[2], ann_total])
print(tableDefaults)

# 4 ---- Best score
scores = np.empty([4])
classifier = ["DT", "SVM", "GNB", "ANN"]
scores[0] = DT_total
scores[1] = svm_total
scores[2] = gnb_total
scores[3] = ann_total
max = scores[0]
max_name = classifier[0]

for i in range(0, 3):
    if scores[i] > max:
        max = scores[i]
        max_name = classifier[i]

print("Seems like the highest score is ", max, "from classifier named", max_name)

# +-----------------+--------------------+--------------------+--------------------+--------------------+
# | Classifier Name |     Precision      |       Recall       |      F-score       |       Score        |
# +-----------------+--------------------+--------------------+--------------------+--------------------+
# |        DT       | 0.8144230769230769 | 0.8097514340344169 | 0.8120805369127517 | 0.7575757575757576 |
# |       SVM       | 0.6480793060718711 |        1.0         | 0.7864661654135339 | 0.6487322201607916 |
# |       GNB       | 0.7361477572559367 | 0.8001912045889101 | 0.7668346312414109 | 0.6852195423623995 |
# |       ANN       | 0.6472772277227723 |        1.0         | 0.7858752817430503 | 0.647495361781076  |
# +-----------------+--------------------+--------------------+--------------------+--------------------+
# Seems like the highest score is  0.7575757575757576 from classifier named DT
