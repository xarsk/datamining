# Define a function that takes as parameters the dataset and an integer.
# The integer determines a feature (column) of the dataset. Function should return statistical measures such as:
# Mean, Std, Mode, Median, Max, Min, Skewness, Kurtosis

from scipy.stats import mode, skew, kurtosis


def descriptiveStatistics(array, feature):
    print("Statistics for column no.", feature)
    print("Mean:", np.mean(array[:, feature], axis=0))
    print("Standard Deviation:", np.std(array[..., feature], axis=0))
    print("Modal Value:", mode(array[..., feature], axis=0)[0], "for", mode(array[..., feature], axis=0)[1], "times")
    print("Median:", np.median(array[..., feature], axis=0))
    print("Max:", np.max(array[..., feature], axis=0))
    print("Min:", np.min(array[..., feature], axis=0))
    print("Skewness:", skew(array[..., feature], axis=0))
    print("Kurtosis:", kurtosis(array[..., feature], axis=0))


descriptiveStatistics(datasetclean, 1)

# Statistics for column no. 1
# Mean: 0.27824111882400976
# Standard Deviation: 0.10078425854188974
# Modal Value: [0.28] for [263] times
# Median: 0.26
# Max: 1.1
# Min: 0.08
# Skewness: 1.576496515957486
# Kurtosis: 5.085204904517855