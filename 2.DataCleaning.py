# The new dataset contains some NaN values. Remove all records with any NaN value.

datasetClean = datadsetFull[~np.isnan(datasetfull).any(axis=1)]
print(datasetClean)

# [[ 7.    0.27  0.36 ...  0.45  8.8   6.  ]
#  [ 6.3   0.3   0.34 ...  0.49  9.5   6.  ]
#  [ 8.1   0.28  0.4  ...  0.44 10.1   6.  ]
#  ...
#  [ 6.5   0.24  0.19 ...  0.46  9.4   6.  ]
#  [ 5.5   0.29  0.3  ...  0.38 12.8   7.  ]
#  [ 6.    0.21  0.38 ...  0.32 11.8   6.  ]]