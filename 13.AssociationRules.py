# Using a dataset about groceries bought in a supermarket and apyori module.
# Find the association rules produced with the apriori algorithm for various values ​​of confidence, support and lift.

import pandas as pd

nan = float('nan')
transactions = pd.read_csv('gdrive/My Drive/Dataset/transactions.csv')
transactions_list = transactions.values.tolist()
transactions_list = [[i for i in j if i == i] for j in transactions_list]

print(transactions_list)

from apyori import apriori

association_rules = list(apriori(transactions_list, min_support=0.02, min_confidence=0.35, min_lift=2, min_length=2))
# support is frequency of appearence, while confience is the probability of buying a while b is bought and finally lift is
print(association_rules)
