# Use the Data Normalization technique and nomralize the data seperately for each feature, except quality feature.

from sklearn import preprocessing
import matplotlib.pyplot as plt

# Init arrays with dataset's size
dataNorm = np.zeros((datasetClean.shape[0], 11))
dataFinalNorm = np.zeros((datasetClean.shape[0], 12))

print("Pre-Normalized Dataset:\n", datasetClean)

# With axis set to 0, it normalizes each feature.
dataNorm[:, 0:11] = preprocessing.normalize(datasetClean[:, 0:11], axis=0, norm='l2')

#  Adding quality feature
dataFinalNorm[:, 0:11] = dataNorm[:, 0:11]
dataFinalNorm[:, 11] = datasetClean[:, 11]

print("Normalized dataset:\n", dataFinalNorm)


# Pre-Normalized Dataset:
# [[ 7.    0.27  0.36 ...  0.45  8.8   1.  ]
#  [ 6.3   0.3   0.34 ...  0.49  9.5   1.  ]
#  [ 8.1   0.28  0.4  ...  0.44 10.1   1.  ]
#  ...
#  [ 6.5   0.24  0.19 ...  0.46  9.4   1.  ]
#  [ 5.5   0.29  0.3  ...  0.38 12.8   1.  ]
#  [ 6.    0.21  0.38 ...  0.32 11.8   1.  ]]
#
#
# Normalized dataset:
#  [[0.01448202 0.01303656 0.01447256 ... 0.012784   0.01187792 1.        ]
#  [0.01303382 0.01448506 0.01366853 ... 0.01392036 0.01282275 1.        ]
#  [0.01675776 0.01351939 0.01608062 ... 0.01249991 0.01363261 1.        ]
#  ...
#  [0.01344759 0.01158805 0.0076383  ... 0.01306809 0.01268778 1.        ]
#  [0.01137873 0.01400223 0.01206047 ... 0.01079538 0.01727697 1.        ]
#  [0.01241316 0.01013954 0.01527659 ... 0.00909085 0.01592721 1.        ]]