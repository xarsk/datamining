# Dataset's last column is each wine's quality rate, graded in a 0 to 10 scale. We consider 0-5 as a bad rating,
# and 6 to 10 as a good one. Replace dataset's last column with 0 if the wine is considered as bad quality,
# or with 1 if the wine is considered as a good quality one.


datasetClean[:, 11][datasetClean[:, 11] <= 5] = 0
datasetClean[:, 11][datasetClean[:, 11] >= 6] = 1
print(datasetClean[:, 11])

# [1. 1. 1. ... 1. 1. 1.]
